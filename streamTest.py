import cv2
import numpy as np
import urllib.request

stream = urllib.request.urlopen('http://10.10.10.187:5000/video_feed')
bytes= b''
while True:
    bytes += stream.read(1024)
    # print(bytes)
    # print('------\n')
    a = bytes.find(b'\xff\xd8')  # JPEG start
    b = bytes.find(b'\xff\xd9')  # JPEG end
    print(a)
    print(b)
    if a!=-1 and b!=-1:
        jpg = bytes[a:b+2] # actual image
        bytes= bytes[b+2:] # other informations

        # decode to colored image ( another option is cv2.IMREAD_GRAYSCALE )
        img = cv2.imdecode(np.fromstring(jpg, dtype=np.uint8),cv2.IMREAD_COLOR)
        cv2.imshow('Window name',img) # display image while receiving data
        if cv2.waitKey(1) =='q': # if user hit esc
            exit(0) # exit program