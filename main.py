# import the necessary modules
import threading
import random
import paho.mqtt.publish as publish
import urllib.request
import socket
import freenect
import cv2
import numpy as np
import time

mqttObject = None
arrow_size = 100
gateNum = -1
r_arrow_up = cv2.imread('arrow_up.png')
r_arrow_up = cv2.resize(r_arrow_up, (arrow_size, arrow_size), interpolation=cv2.INTER_CUBIC)
r_arrow_down = cv2.imread('arrow_down.png')
r_arrow_down = cv2.resize(r_arrow_down, (arrow_size, arrow_size), interpolation=cv2.INTER_CUBIC)
l_arrow_up = r_arrow_up.copy()
l_arrow_down = r_arrow_down.copy()
r_up_bool = False
r_down_bool = False
l_up_bool = False
l_down_bool = False
gateNumTable = {'1': 1, '2': 2}
videoStreaming = []
streamingLife = True
r_up_x = 350
r_up_y = 70
r_down_x = 360
r_down_y = 150
l_up_x = 190
l_up_y = 70
l_down_x = 180
l_down_y = 150
x_bios = 20
y_bios = 20


def get_ip_address():
    ip_address = ''
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    ip_address = s.getsockname()[0]
    s.close()
    return ip_address


# function to get RGB image from kinect
def get_video():
    array, _ = freenect.sync_get_video()
    array = cv2.cvtColor(array, cv2.COLOR_RGB2BGR)
    return array


# function to get depth image from kinect
def get_depth(max, min):
    array, _ = freenect.sync_get_depth()
    array[array < min] = 0
    array[array > max] = 0
    array = array.astype(np.uint8)
    return array


def combineImage(originalImage, addImage, yStart, xStart, weigth):
    roi = originalImage[yStart:yStart + arrow_size, xStart:xStart + arrow_size]
    dst = cv2.addWeighted(roi, 1, addImage, weigth, 0)
    originalImage[yStart:yStart + arrow_size, xStart:xStart + arrow_size] = dst
    return originalImage


def updateDepth(depthImage):
    global r_up_bool, r_down_bool, l_up_bool, l_down_bool

    depthImage = cv2.flip(depthImage, 1)

    if countEnoughRate(depthImage[r_up_y + y_bios: r_up_y + arrow_size - y_bios
                       , r_up_x + x_bios:r_up_x + arrow_size - x_bios]) > 0.08:
        r_up_bool = True
    else:
        r_up_bool = False

    if countEnoughRate(depthImage[r_down_y + y_bios: r_down_y + arrow_size - y_bios
                       , r_down_x + x_bios:r_down_x + arrow_size - x_bios]) > 0.08:
        r_down_bool = True
    else:
        r_down_bool = False

    if countEnoughRate(depthImage[l_up_y + y_bios:l_up_y + arrow_size - y_bios
                       , l_up_x + x_bios:l_up_x + arrow_size - x_bios]) > 0.08:
        l_up_bool = True
    else:
        l_up_bool = False

    if countEnoughRate(depthImage[l_down_y + y_bios:l_down_y + arrow_size - y_bios
                       , l_down_x + x_bios:l_down_x + arrow_size - x_bios]) > 0.08:
        l_down_bool = True
    else:
        l_down_bool = False
    # print(r_up_bool, r_down_bool, l_up_bool, l_down_bool)


def updateFrame(frame):
    global r_up_bool, r_down_bool, l_up_bool, l_down_bool, videoStreaming
    frame = cv2.flip(frame, 1)

    if r_up_bool:
        frame = combineImage(frame, r_arrow_up[:, :, ::-1], r_up_y, r_up_x, 1)
    else:
        frame = combineImage(frame, r_arrow_up, r_up_y, r_up_x, 1)

    if r_down_bool:
        frame = combineImage(frame, r_arrow_down[:, :, ::-1], r_down_y, r_down_x, 1)
    else:
        frame = combineImage(frame, r_arrow_down, r_down_y, r_down_x, 1)

    if l_up_bool:
        frame = combineImage(frame, l_arrow_up[:, :, ::-1], l_up_y, l_up_x, 1)
    else:
        frame = combineImage(frame, l_arrow_up, l_up_y, l_up_x, 1)

    if l_down_bool:
        frame = combineImage(frame, l_arrow_down[:, :, ::-1], l_down_y, l_down_x, 1)
    else:
        frame = combineImage(frame, l_arrow_down, l_down_y, l_down_x, 1)
    if gateNum == -1:
        cv2.putText(frame, '', (305, 80), cv2.FONT_HERSHEY_SIMPLEX,
                    1.5, (255, 255, 40), 2, cv2.LINE_AA)
    else:
        cv2.putText(frame, str(gateNum), (305, 80), cv2.FONT_HERSHEY_SIMPLEX,
                    1, (255, 255, 40), 2, cv2.LINE_AA)

    cv2.putText(frame, 'L', (100, 160), cv2.FONT_HERSHEY_SIMPLEX,
                3, (175, 255,), 2, cv2.LINE_AA)
    cv2.putText(frame, 'R', (480, 160), cv2.FONT_HERSHEY_SIMPLEX,
                3, (175, 255,), 2, cv2.LINE_AA)

    cv2.putText(frame, 'GATE', (264, 40), cv2.FONT_HERSHEY_SIMPLEX,
                1.5, (255, 255, 40), 2, cv2.LINE_AA)

    monitorBox = cv2.resize(videoStreaming, (256, 192), interpolation=cv2.INTER_CUBIC)
    frame[288:, 192:448] = monitorBox
    return frame


def countEnoughRate(image):
    x, y = image.shape
    maxWhite = x * y * 255
    total = sum(map(sum, image))
    return total / maxWhite


def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))
    # 連接完成之後訂閱gpio主题
    client.subscribe("toKinect")


def randomGateNum():
    global gateNumTable
    gate1Num = random.randint(1, 9)
    gate2Num = random.randint(1, 9)
    while gate2Num == gate1Num:
        gate2Num = random.randint(1, 8)
    gateNumTable['1'] = gate1Num
    gateNumTable['2'] = gate2Num


def on_message(client, userdata, msg):
    global gateNumTable, gateNum
    msg_str = msg.payload.decode("utf8")
    msgs = msg_str.split(',')
    if len(msgs) <= 1:
        gateNum = -1
    elif gateNum == -1:
        randomGateNum()
        gateNum = gateNumTable[str(msgs[1])]
        message = '1,' + str(gateNumTable['1']) + ',2,' + str(gateNumTable['2'])
        publish.single("toCenter", message, qos=1, hostname=get_ip_address())


def mqtt():
    global mqttObject
    import paho.mqtt.client as mqtt
    client = mqtt.Client()
    mqttObject = client
    client.on_connect = on_connect
    client.on_message = on_message
    try:
        # broker server tcp
        # client.connect('127.0.0.1', 1883, 60)
        client.connect(get_ip_address(), 1883, 60)
        client.loop_forever()
    except:
        client.disconnect()


def carController():
    global r_up_bool, r_down_bool, l_up_bool, l_down_bool
    r_speed = 0
    l_speed = 0
    speed = 60
    if r_up_bool:
        r_speed = speed
    elif r_down_bool:
        r_speed = -speed
    if l_up_bool:
        l_speed = speed
    elif l_down_bool:
        l_speed = -speed
    message = 'm,l,' + str(l_speed) + ',r,' + str(r_speed)
    publish.single("toCar2", message, qos=1, hostname=get_ip_address())


def getVideoStreaming():
    global videoStreaming
    # stream = urllib.request.urlopen('http://10.10.10.187:5000/video_feed', timeout=2)
    # bytes = b''

    while streamingLife:
        try:
            bytes += stream.read(1024)
            # print('------\n')
            a = bytes.find(b'\xff\xd8')  # JPEG start
            b = bytes.find(b'\xff\xd9')  # JPEG end
            if a != -1 and b != -1:
                jpg = bytes[a:b + 2]  # actual image
                bytes = bytes[b + 2:]  # other informations
                # decode to colored image ( another option is cv2.IMREAD_GRAYSCALE )
                videoStreaming = cv2.imdecode(np.fromstring(jpg, dtype=np.uint8), cv2.IMREAD_COLOR)
        except Exception as e1:
            try:
                stream = urllib.request.urlopen('http://10.10.10.187:5000/video_feed', timeout=2)
                bytes = b''
            except Exception as e2:
                print('Waiting for video streaming...')
                time.sleep(2)


def erode(grayImage, kernelSize): #膨脹
    element = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (kernelSize, kernelSize))
    output = cv2.erode(grayImage, element)
    return output

def dilate(grayImage, kernelSize): #侵蝕
    element = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (kernelSize, kernelSize))
    output = cv2.dilate(grayImage,element)
    return output

if __name__ == "__main__":

    mqtt_thread = threading.Thread(target=mqtt)  ##異步
    mqtt_thread.start()
    videoStreaming_thread = threading.Thread(target=getVideoStreaming)
    videoStreaming_thread.start()

    cv2.namedWindow("FullScreen", cv2.WND_PROP_FULLSCREEN)
    cv2.setWindowProperty("FullScreen", cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
    while len(videoStreaming) == 0:
        continue

    # w, h, c = 640, 810, 3
    # finalImage = np.zeros([810,640,3],dtype=np.uint8)
    while 1:
        # frame = get_video()
        # depth1 = get_depth(800, 100)
        depth = get_depth(1010, 900)
        depth[:300, 500:] = 0
        depth = dilate(depth, 3)
        # depth = erode(depth, 3)
        frame = cv2.cvtColor(depth, cv2.COLOR_GRAY2RGB)
        updateDepth(depth)

        frame = updateFrame(frame)
        # finalImage[:330,:] = frame
        # finalImage[330:,:] = videoStreaming
        cv2.imshow('FullScreen', frame)
        carController()
        # quit program when 'esc' key is pressed
        k = cv2.waitKey(5) & 0xFF
        if k == 27:
            break
    mqttObject.disconnect()
    streamingLife = False
    cv2.destroyAllWindows()
